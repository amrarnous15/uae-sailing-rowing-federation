'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
var rename = require("gulp-rename");
const image = require('gulp-image');
const browserSync = require('browser-sync').create();
gulp.task( 'buildStyles', () => {
  return gulp.src('./scss/**/*.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(autoprefixer('last 2 versions'))
    .pipe(rename({ extname: '.min.css' }))
    .pipe(gulp.dest('./css'))

});
gulp.task('minifyImgs', () => {
  gulp.src('./imgs/tominify/*')
    .pipe(image())
    .pipe(gulp.dest('./imgs'));
});

gulp.task('serve', () => {
  browserSync.init({
    server: {
      baseDir: './',
      index: 'index.html'
    },
    notify: false,
    injectChanges: true
  });
 gulp.watch('./scss/**/*.scss', gulp.series('buildStyles'));
  gulp.watch('./imgs/tominify/*', gulp.series('minifyImgs'));
  gulp.watch('./**/*').on('change', browserSync.reload);
});


gulp.task('default', gulp.series('serve'));
