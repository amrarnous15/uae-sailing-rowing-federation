var boardMembersSlider = new Glide('.board-members-slider', {
  type: 'carousel',

  perView: 4,
  gap: 25,
   breakpoints: {
    1024: {
      perView: 2
    },
    600: {
      perView: 1
    }
  }
});

var membersSlider = new Glide('.members-slider', {
  type: 'carousel',
  perView: 6,
  gap: 25,
  breakpoints: {
    1024: {
      perView: 1
    },
    600: {
      perView: 2,
      gap: 5
    }
  }
});
var newsSlider = new Glide('.news-slider', {
  type: 'carousel',
  gap: -30,
  perView: 6,
  peek: -window.innerWidth*0.55,
  startAt: 1,
  focusAt: 'center',
  rewind: true,
  breakpoints: {
    1024: {
      perView: 2,
      peek: 0,

    },
    600: {
      perView:1,
      peek: 0
    }
  }
});
boardMembersSlider.mount();
membersSlider.mount();
newsSlider.mount();
