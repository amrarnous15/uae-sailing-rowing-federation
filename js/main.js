const navbar = document.querySelector('.navbar');
const navbarToggler = document.getElementById('navbarSupportedContent');

// Add background to navbar when user scroll
window.onscroll = () => {
  if (window.pageYOffset >= 150){
    navbar.classList.add('bg-dark-primary');
  } else {
    if (!navbarToggler.classList.contains('show')) {
      navbar.classList.remove('bg-dark-primary');
    }
  }
}
navbarToggler.addEventListener('show.bs.collapse', function () {
    if (!navbar.classList.contains('bg-dark-primary')){ // add background to navbar when user toggle menu on small screens if the background is transparent
      navbar.classList.add('bg-dark-primary');
    }
});
navbarToggler.addEventListener('hide.bs.collapse', function () {
    if (window.pageYOffset < 150){
      navbar.classList.remove('bg-dark-primary');
    }
});