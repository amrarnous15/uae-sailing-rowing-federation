# 1. Clone The Project

```
git clone https://gitlab.com/amrarnous15/uae-sailing-rowing-federation
```

# 2. run npm install to install the required dependencies

```
npm i
```

# 3. run the Project using gulp

```
gulp
```
